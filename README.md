# reversi-othello-java-console-simple-app
Reversi(Othello) Simple Java console application

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Running the application locally

Execute the main method in the com.demo.MyReversi_2player class from your IDE.

OR

run following the jar package in shell

- if console cannot read Unicode4.1.0 version, the represented display will othello chips with 
"X" for black  and "O" for white 

```shell
$ java -jar reversi-assignment-run-no-unicode.jar
```

- if console can read Unicode4.1.0 version, the represented display will othello chips with balck and white circle

```shell
$ java -jar reversi-assignment.jar
```
