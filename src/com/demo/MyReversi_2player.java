package com.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

public class MyReversi_2player {

	static final String constFor_X = "\u2B24"; // "X"; // black
	static final String constFor_O = "\u2B58"; // "O"; // white

	static final String constFor_X_black_string = "Balck";// "X";
	static final String constFor_O_white_string = "white";// "O";

	static final String constFor_DRAW = "DRAW";
	static final String constSpace = "\t";
	static final String emptySlot = "\u002D";// "-";

	static String turn;

	private static final int a = 0, b = 1, c = 2, d = 3, e = 4, f = 5, g = 6,
			h = 7;

	static String[][] constToEnter = {
			{ "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1" },
			{ "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2" },
			{ "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3" },
			{ "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4" },
			{ "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5" },
			{ "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6" },
			{ "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7" },
			{ "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8" } };

	static String[][] liveReversiBoard = {
			{ emptySlot, emptySlot, emptySlot, emptySlot, emptySlot, emptySlot,
					emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, emptySlot, emptySlot, emptySlot,
					emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, emptySlot, emptySlot, emptySlot,
					emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, constFor_O, constFor_X,
					emptySlot, emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, constFor_X, constFor_O,
					emptySlot, emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, emptySlot, emptySlot, emptySlot,
					emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, emptySlot, emptySlot, emptySlot,
					emptySlot, emptySlot },
			{ emptySlot, emptySlot, emptySlot, emptySlot, emptySlot, emptySlot,
					emptySlot, emptySlot } };

	public static void main(String[] args) {
		porceedHumanVsHumanMode();

	}

	public static void porceedHumanVsHumanMode() {
		String winner = null;

		turn = constFor_X; // start with X

		System.out.println("The game starts.");
		printCurrentSituationOfBoard();
		String[] allSlots = convertTwoRowtoOneArr(constToEnter);

		try (Scanner in = new Scanner(System.in)) {

			winner = checkWinnner();

			while (winner == null) {

				// player turn message
				// System.out.println(" messagePlayerTurn ... ");
				messagePlayerTurn(turn);

				String cellInput = in.nextLine();
				cellInput = cellInput.trim();
				System.out.println(messageInputPosition(cellInput));

				// illegal inputs // not (a-h 1-8)
				boolean contains = Arrays.stream(allSlots).anyMatch(
						cellInput::equalsIgnoreCase);
				if (!contains) {
					messageIllegalInput();
					continue;
				}

				// input index is not empty -- already has white/black chip
				boolean isEmptySlot = checkInputIndexIsEmptySlot(cellInput);
				if (!isEmptySlot) {
					messageIllegalInput();
					messagePlayerTurn(turn);
					messageIllegalInputAgain();
					continue;
				}

				// input index on empty surrounding neighbour
				boolean isAllNeighbourEmpty = checkInputPlaceNeighbourAreAllEmpty(cellInput);
				if (isAllNeighbourEmpty) {
					messageIllegalInput();
					messagePlayerTurn(turn);
					messageIllegalInputAgain();
					continue;
				}

				// input index is not near opposite color but same color --
				// illegal place
				boolean isAllNeighbourHasNoOppositeColor = checkInputPlaceNeighbourHasNoOppositeColor(cellInput);
				if (!isAllNeighbourHasNoOppositeColor) {
					messageIllegalInput();
					messagePlayerTurn(turn);
					messageIllegalInputAgain();
					continue;
				}

				// input index is near opposite color but cannot place on there
				// because cannot
				// flip opposite color
				boolean isAllNeighbourHasOppositeColorButCannotFlip = checkInputPlaceNeighbourHasOppositeColorButCannotFlip(cellInput);
				if (isAllNeighbourHasOppositeColorButCannotFlip) {
					messageIllegalInput();
					messagePlayerTurn(turn);
					messageIllegalInputAgain();
					continue;
				}

				int tempY = inputFirstCharToInt(cellInput);
				int tempX = inputSecondCharToInt(cellInput);

				if (liveReversiBoard[tempX][tempY].equals(emptySlot)) {

					liveReversiBoard[tempX][tempY] = turn;

					// print input index
					printCurrentSituationOfBoard();
					checkInputPlaceNeighbourAndFlip(cellInput);

					// print flipped outcome
					printCurrentSituationOfBoard();
					winner = checkWinnner();

					if (Objects.isNull(winner)) {
						turn = turn.equals(constFor_X) ? constFor_O
								: constFor_X;
						// System.out.println("line 148 ... ");
						// messagePlayerTurn(turn);

						Boolean playerHasNoMove = proceedHasMove(turn);
						if (playerHasNoMove) {
							System.out.println(messageIllegalNoMoreMove(turn));
							turn = turn.equals(constFor_X) ? constFor_O
									: constFor_X;

							messagePlayerTurn(turn);
						}
					}

				}

			}
		}

	}

	public static String checkWinnner() {

		String winner = null;

		List<String> onBoard_O_Data = new ArrayList<String>();
		List<String> onBoard_X_Data = new ArrayList<String>();
		List<String> onBoardNotEmptyData = new ArrayList<String>();

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				String boardData = liveReversiBoard[i][j];

				if (!boardData.equals(emptySlot)) {
					onBoardNotEmptyData.add(liveReversiBoard[i][j]);
				}

				if (boardData.equals(constFor_O)) {
					onBoard_O_Data.add(liveReversiBoard[i][j]);
				}
				if (boardData.equals(constFor_X)) {
					onBoard_X_Data.add(liveReversiBoard[i][j]);
				}
			}
		}

		if (onBoardNotEmptyData.size() == 64) {
			winner = updateWinner(onBoard_O_Data, onBoard_X_Data);
		} else {
			Boolean bothPlayerHasNoMoves = checkBothCanMove(onBoardNotEmptyData);
			if (bothPlayerHasNoMoves) {
				winner = updateWinner(onBoard_O_Data, onBoard_X_Data);
			}
		}

		return winner;

	}

	public static String updateWinner(List<String> onBoard_O_Data,
			List<String> onBoard_X_Data) {
		String winner = null;
		int countX = onBoard_X_Data.size();
		int countO = onBoard_O_Data.size();

		if (countO > countX) {
			messageResultWinner(onBoard_O_Data.size(), onBoard_X_Data.size());
			messageGameOverOnePlayerWin(constFor_O_white_string);
			winner = constFor_O;

		} else if (countO < countX) {
			messageResultWinner(onBoard_O_Data.size(), onBoard_X_Data.size());
			messageGameOverOnePlayerWin(constFor_X_black_string);
			winner = constFor_X;
		} else if (countO == countX) {
			messageGameOverDraw();
			messageResultWinner(onBoard_O_Data.size(), onBoard_X_Data.size());
			winner = constFor_DRAW;
		} else {
			winner = null;
		}
		return winner;
	}

	private static boolean checkInputIndexIsEmptySlot(String cellInput) {
		int inputTempX = inputSecondCharToInt(cellInput);
		int inputTempY = inputFirstCharToInt(cellInput);

		return liveReversiBoard[inputTempX][inputTempY].equals(emptySlot);
	}

	private static boolean checkInputPlaceNeighbourHasOppositeColorButCannotFlip(
			String cellInput) {

		int inputTempX = inputSecondCharToInt(cellInput);
		int inputTempY = inputFirstCharToInt(cellInput);

		List<String> toTop = new ArrayList<>();
		List<String> toDown = new ArrayList<>();
		List<String> toLeft = new ArrayList<>();
		List<String> toRight = new ArrayList<>();
		List<String> toTopLeft = new ArrayList<>();
		List<String> toTopRight = new ArrayList<>();
		List<String> toDownLeft = new ArrayList<>();
		List<String> toDownRight = new ArrayList<>();

		get8DirectionForCurrentIndex(inputTempX, inputTempY, toTop, toDown,
				toLeft, toRight, toTopLeft, toTopRight, toDownLeft, toDownRight);

		List<Boolean> isAllTopIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllLeftIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllTopLeftIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllTopRightIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllDownIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllRightIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllDownLeftIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllDownRightIsEmpty = new ArrayList<Boolean>();

		boolean allTopIsEmpty = false; // all top are empty
		boolean allDownRightIsEmpty = false;
		boolean allLeftIsEmpty = false;
		boolean allTopLeftIsEmpty = false;
		boolean allTopRightIsEmpty = false;
		boolean allDownIsEmpty = false;
		boolean allRightIsEmpty = false;
		boolean allDownLeftIsEmpty = false;

		Map<String, List<String>> arrToCheckColorAfterCheckedEmpty = new HashMap<String, List<String>>();

		for (String string : toTop) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllTopIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allTopIsEmpty = isAllTopIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allTopIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toTop",
					new ArrayList<String>());
		}

		//
		for (String string : toLeft) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllLeftIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allLeftIsEmpty = isAllLeftIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allLeftIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toLeft",
					new ArrayList<String>());
		}

		//
		for (String string : toTopLeft) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllTopLeftIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allTopLeftIsEmpty = isAllTopLeftIsEmpty.stream().allMatch(
				Boolean::valueOf);

		if (!allTopLeftIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toTopLeft",
					new ArrayList<String>());
		}

		//
		for (String string : toTopRight) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllTopRightIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allTopRightIsEmpty = isAllTopRightIsEmpty.stream().allMatch(
				Boolean::valueOf);
		if (!allTopRightIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toTopRight",
					new ArrayList<String>());
		}

		//
		for (String string : toDown) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllDownIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allDownIsEmpty = isAllDownIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allDownIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toDown",
					new ArrayList<String>());
		}

		//
		for (String string : toRight) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllRightIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allRightIsEmpty = isAllRightIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allRightIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toRight",
					new ArrayList<String>());
		}

		//
		for (String string : toDownLeft) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllDownLeftIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allDownLeftIsEmpty = isAllDownLeftIsEmpty.stream().allMatch(
				Boolean::valueOf);
		if (!allDownLeftIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toDownLeft",
					new ArrayList<String>());
		}

		//
		for (String string : toDownRight) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllDownRightIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allDownRightIsEmpty = isAllDownRightIsEmpty.stream().allMatch(
				Boolean::valueOf);
		if (!allDownRightIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toDownRight",
					new ArrayList<String>());
		}
		//

		boolean isSurrounding8DirectionIsEmpty = allTopIsEmpty
				&& allTopLeftIsEmpty && allLeftIsEmpty && allDownLeftIsEmpty
				&& allDownIsEmpty && allDownRightIsEmpty && allRightIsEmpty
				&& allTopRightIsEmpty ? true : false;

		String colorOfOpositeTurn = turn.equals(constFor_X) ? constFor_O
				: constFor_X;

		List<String> checkCanFlipMapKey = new ArrayList<String>();

		if (!isSurrounding8DirectionIsEmpty) {
			// get false
			// System.out.println("## NOT all 8 direction is EMPTY ");

			if (!allTopIsEmpty) {
				// System.out.println("allTopIsEmpty : " + allTopIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toTop").addAll(
						checkFlippable(toTop));
				checkCanFlipMapKey.add("toTop");
			}
			if (!allTopLeftIsEmpty) {
				// System.out.println("allTopLeftIsEmpty : " +
				// allTopLeftIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toTopLeft").addAll(
						checkFlippable(toTopLeft));
				checkCanFlipMapKey.add("toTopLeft");
			}
			if (!allLeftIsEmpty) {
				// System.out.println("allLeftIsEmpty : " + allLeftIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toLeft").addAll(
						checkFlippable(toLeft));
				checkCanFlipMapKey.add("toLeft");
			}
			if (!allDownLeftIsEmpty) {
				// System.out.println("allDownLeftIsEmpty : " +
				// allDownLeftIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toDownLeft").addAll(
						checkFlippable(toDownLeft));
				checkCanFlipMapKey.add("toDownLeft");
			}
			if (!allDownIsEmpty) {
				// System.out.println("allDownIsEmpty : " + allDownIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toDown").addAll(
						checkFlippable(toDown));
				checkCanFlipMapKey.add("toDown");
			}
			if (!allDownRightIsEmpty) {
				// System.out.println("allDownRightIsEmpty : " +
				// allDownRightIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toDownRight").addAll(
						checkFlippable(toDownRight));
				checkCanFlipMapKey.add("toDownRight");
			}
			if (!allRightIsEmpty) {
				// System.out.println("allRightIsEmpty : " + allRightIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toRight").addAll(
						checkFlippable(toRight));
				checkCanFlipMapKey.add("toRight");
			}
			if (!allTopRightIsEmpty) {
				// System.out.println("allTopRightIsEmpty : " +
				// allTopRightIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toTopRight").addAll(
						checkFlippable(toTopRight));
				checkCanFlipMapKey.add("toTopRight");
			}

			// System.out.println("arrToCheckColorAfterCheckedEmpty " +
			// arrToCheckColorAfterCheckedEmpty.size() + " ---- "
			// + checkCanFlipMapKey);
			// System.out.println(arrToCheckColorAfterCheckedEmpty);

			List<Boolean> unableToMove = new ArrayList<Boolean>();

			for (String mapKey : checkCanFlipMapKey) {

				List<String> firstColorIndex = arrToCheckColorAfterCheckedEmpty
						.get(mapKey);
				if (firstColorIndex.get(0).equals(turn)) {
					// System.out.println(
					// "############ next first is " + firstColorIndex.get(0) +
					// " & next index is same color");
					unableToMove.add(true);
				} else {
					// System.out.println("############ next first is " +
					// firstColorIndex.get(0));

					Boolean tempIsAllOppositeColor = firstColorIndex.stream()
							.allMatch(a -> a.equals(colorOfOpositeTurn));
					unableToMove.add(tempIsAllOppositeColor);

					if (!tempIsAllOppositeColor) {

					}

				}
			}

			// System.out.println("unableToMove " + unableToMove);

			boolean neighbourButCannotMove = unableToMove.stream().allMatch(
					Boolean::valueOf);

			if (neighbourButCannotMove) {
				return true;
			} else {
				return false;
			}

		} else {
			// System.out.println("##  all 8 direction is EMPTY ");
			return true;
		}

	}

	private static void checkInputPlaceNeighbourAndFlip(String cellInput) {

		int inputTempX = inputSecondCharToInt(cellInput);
		int inputTempY = inputFirstCharToInt(cellInput);

		List<String> toTop = new ArrayList<>();
		List<String> toDown = new ArrayList<>();
		List<String> toLeft = new ArrayList<>();
		List<String> toRight = new ArrayList<>();
		List<String> toTopLeft = new ArrayList<>();
		List<String> toTopRight = new ArrayList<>();
		List<String> toDownLeft = new ArrayList<>();
		List<String> toDownRight = new ArrayList<>();

		get8DirectionForCurrentIndex(inputTempX, inputTempY, toTop, toDown,
				toLeft, toRight, toTopLeft, toTopRight, toDownLeft, toDownRight);

		List<Boolean> isAllTopIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllLeftIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllTopLeftIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllTopRightIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllDownIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllRightIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllDownLeftIsEmpty = new ArrayList<Boolean>();
		List<Boolean> isAllDownRightIsEmpty = new ArrayList<Boolean>();

		boolean allTopIsEmpty = false; // all top are empty
		boolean allDownRightIsEmpty = false;
		boolean allLeftIsEmpty = false;
		boolean allTopLeftIsEmpty = false;
		boolean allTopRightIsEmpty = false;
		boolean allDownIsEmpty = false;
		boolean allRightIsEmpty = false;
		boolean allDownLeftIsEmpty = false;

		Map<String, List<String>> arrToCheckColorAfterCheckedEmpty = new HashMap<String, List<String>>();

		for (String string : toTop) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllTopIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allTopIsEmpty = isAllTopIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allTopIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toTop",
					new ArrayList<String>());
		}

		//
		for (String string : toLeft) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllLeftIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allLeftIsEmpty = isAllLeftIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allLeftIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toLeft",
					new ArrayList<String>());
		}

		//
		for (String string : toTopLeft) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllTopLeftIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allTopLeftIsEmpty = isAllTopLeftIsEmpty.stream().allMatch(
				Boolean::valueOf);

		if (!allTopLeftIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toTopLeft",
					new ArrayList<String>());
		}

		//
		for (String string : toTopRight) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllTopRightIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allTopRightIsEmpty = isAllTopRightIsEmpty.stream().allMatch(
				Boolean::valueOf);
		if (!allTopRightIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toTopRight",
					new ArrayList<String>());
		}

		//
		for (String string : toDown) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllDownIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allDownIsEmpty = isAllDownIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allDownIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toDown",
					new ArrayList<String>());
		}

		//
		for (String string : toRight) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllRightIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allRightIsEmpty = isAllRightIsEmpty.stream().allMatch(Boolean::valueOf);
		if (!allRightIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toRight",
					new ArrayList<String>());
		}

		//
		for (String string : toDownLeft) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllDownLeftIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allDownLeftIsEmpty = isAllDownLeftIsEmpty.stream().allMatch(
				Boolean::valueOf);
		if (!allDownLeftIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toDownLeft",
					new ArrayList<String>());
		}

		//
		for (String string : toDownRight) {
			String tempObj = liveReversiBoard[Character.getNumericValue(string
					.charAt(0))][Character.getNumericValue(string.charAt(1))];
			isAllDownRightIsEmpty.add(tempObj.equals(emptySlot) ? true : false);
		}
		allDownRightIsEmpty = isAllDownRightIsEmpty.stream().allMatch(
				Boolean::valueOf);
		if (!allDownRightIsEmpty) {
			arrToCheckColorAfterCheckedEmpty.put("toDownRight",
					new ArrayList<String>());
		}
		//

		boolean isSurrounding8DirectionIsEmpty = allTopIsEmpty
				&& allTopLeftIsEmpty && allLeftIsEmpty && allDownLeftIsEmpty
				&& allDownIsEmpty && allDownRightIsEmpty && allRightIsEmpty
				&& allTopRightIsEmpty ? true : false;

		String colorOfOpositeTurn = turn.equals(constFor_X) ? constFor_O
				: constFor_X;

		List<String> checkCanFlipMapKey = new ArrayList<String>();

		if (!isSurrounding8DirectionIsEmpty) {
			// get false
			// System.out.println("## NOT all 8 direction is EMPTY ");

			if (!allTopIsEmpty) {
				// System.out.println("allTopIsEmpty : " + allTopIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toTop").addAll(
						checkFlippable(toTop));
				checkCanFlipMapKey.add("toTop");
			}
			if (!allTopLeftIsEmpty) {
				// System.out.println("allTopLeftIsEmpty : " +
				// allTopLeftIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toTopLeft").addAll(
						checkFlippable(toTopLeft));
				checkCanFlipMapKey.add("toTopLeft");
			}
			if (!allLeftIsEmpty) {
				// System.out.println("allLeftIsEmpty : " + allLeftIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toLeft").addAll(
						checkFlippable(toLeft));
				checkCanFlipMapKey.add("toLeft");
			}
			if (!allDownLeftIsEmpty) {
				// System.out.println("allDownLeftIsEmpty : " +
				// allDownLeftIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toDownLeft").addAll(
						checkFlippable(toDownLeft));
				checkCanFlipMapKey.add("toDownLeft");
			}
			if (!allDownIsEmpty) {
				// System.out.println("allDownIsEmpty : " + allDownIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toDown").addAll(
						checkFlippable(toDown));
				checkCanFlipMapKey.add("toDown");
			}
			if (!allDownRightIsEmpty) {
				// System.out.println("allDownRightIsEmpty : " +
				// allDownRightIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toDownRight").addAll(
						checkFlippable(toDownRight));
				checkCanFlipMapKey.add("toDownRight");
			}
			if (!allRightIsEmpty) {
				// System.out.println("allRightIsEmpty : " + allRightIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toRight").addAll(
						checkFlippable(toRight));
				checkCanFlipMapKey.add("toRight");
			}
			if (!allTopRightIsEmpty) {
				// System.out.println("allTopRightIsEmpty : " +
				// allTopRightIsEmpty);
				arrToCheckColorAfterCheckedEmpty.get("toTopRight").addAll(
						checkFlippable(toTopRight));
				checkCanFlipMapKey.add("toTopRight");
			}

			// System.out.println("arrToCheckColorAfterCheckedEmpty " +
			// arrToCheckColorAfterCheckedEmpty.size() + " ---- "
			// + checkCanFlipMapKey);
			// System.out.println(arrToCheckColorAfterCheckedEmpty);

			for (String mapKey : checkCanFlipMapKey) {

				List<String> colors = arrToCheckColorAfterCheckedEmpty
						.get(mapKey);
				if (!colors.get(0).equals(turn)) {
					// System.out.println("############ next first is " +
					// colors.get(0));

					Boolean tempIsAllOppositeColor = colors.stream().allMatch(
							a -> a.equals(colorOfOpositeTurn));

					Integer firstIndex = colors.stream()
							.filter(v -> v.contains(turn))
							.map(v -> colors.indexOf(v)).findFirst().orElse(-1);
					// System.out.println("firstIndex: " + firstIndex);

					if (!tempIsAllOppositeColor) {

						if (mapKey.equalsIgnoreCase("toTop")) {
							// System.out.println("toTop" + toTop);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toTop.get(i).charAt(0))][Character
										.getNumericValue(toTop.get(i).charAt(1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toDown")) {
							// System.out.println("toDown" + toDown);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toDown.get(i)
												.charAt(0))][Character
										.getNumericValue(toDown.get(i)
												.charAt(1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toLeft")) {
							// System.out.println("toLeft" + toLeft);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toLeft.get(i)
												.charAt(0))][Character
										.getNumericValue(toLeft.get(i)
												.charAt(1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toRight")) {
							// System.out.println("toRight" + toRight);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toRight.get(i).charAt(
												0))][Character
										.getNumericValue(toRight.get(i).charAt(
												1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toTopLeft")) {
							// System.out.println("toTopLeft" + toTopLeft);
							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toTopLeft.get(i)
												.charAt(0))][Character
										.getNumericValue(toTopLeft.get(i)
												.charAt(1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toTopRight")) {
							// System.out.println("toTopRight" + toTopRight);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toTopRight.get(i)
												.charAt(0))][Character
										.getNumericValue(toTopRight.get(i)
												.charAt(1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toDownLeft")) {
							// System.out.println("toDownLeft" + toDownLeft);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toDownLeft.get(i)
												.charAt(0))][Character
										.getNumericValue(toDownLeft.get(i)
												.charAt(1))] = turn;
							}

						} else if (mapKey.equalsIgnoreCase("toDownRight")) {
							// System.out.println("toDownRight" + toDownRight);

							for (int i = 0; i <= firstIndex; i++) {
								liveReversiBoard[Character
										.getNumericValue(toDownRight.get(i)
												.charAt(0))][Character
										.getNumericValue(toDownRight.get(i)
												.charAt(1))] = turn;
							}

						} else {
							System.out.println("something went wrong !");
						}

					}
				}
			}

		}

	}

	public static List<String> checkFlippable(List<String> arrayToFlip) {

		List<String> colorList = new ArrayList<String>();
		for (String string : arrayToFlip) {
			String currentColor = liveReversiBoard[Character
					.getNumericValue(string.charAt(0))][Character
					.getNumericValue(string.charAt(1))];
			if (!currentColor.equals(emptySlot)) {
				colorList.add(currentColor);
			}
		}
		// System.out.println("color list " + colorList);
		return colorList;
	}

	public static void get8DirectionForCurrentIndex(int inputTempX,
			int inputTempY, List<String> toTop, List<String> toDown,
			List<String> toLeft, List<String> toRight, List<String> toTopLeft,
			List<String> toTopRight, List<String> toDownLeft,
			List<String> toDownRight) {
		try {

			for (int i = 0; i < inputTempX; i++) {
				toTop.add(i + "" + inputTempY);
			}
			for (int i = inputTempX + 1; i < 8; i++) {
				toDown.add(i + "" + inputTempY);
			}
			for (int i = 0; i < inputTempY; i++) {
				toLeft.add(inputTempX + "" + i);
			}
			for (int i = inputTempY + 1; i < 8; i++) {
				toRight.add(inputTempX + "" + i);
			}

			for (int i = 1; i <= inputTempX; i++) {
				if (inputTempX - i >= 0 && inputTempY - i >= 0) {
					toTopLeft.add((inputTempX - i) + "" + (inputTempY - i));
				}
			}

			for (int i = 1; i <= inputTempX; i++) {
				if (inputTempX - i >= 0 && inputTempY + i <= 7) {
					toTopRight.add((inputTempX - i) + "" + (inputTempY + i));
				}
			}

			for (int i = 1; i < 8 - inputTempX; i++) {
				if (inputTempX + i <= 7 && inputTempY - i >= 0) {
					toDownLeft.add((inputTempX + i) + "" + (inputTempY - i));
				}
			}
			for (int i = 1; i < 8 - inputTempX; i++) {
				if (inputTempX + i <= 7 && inputTempY + i <= 7) {
					toDownRight.add((inputTempX + i) + "" + (inputTempY + i));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		Collections.sort(toTop, Collections.reverseOrder());
		Collections.sort(toLeft, Collections.reverseOrder());
		Collections.sort(toTopLeft, Collections.reverseOrder());
		Collections.sort(toTopRight, Collections.reverseOrder());
		Collections.sort(toDown);
		Collections.sort(toRight);
		Collections.sort(toDownLeft);
		Collections.sort(toDownRight);
	}

	private static boolean checkInputPlaceNeighbourHasNoOppositeColor(
			String cellInput) {

		List<String> neighbourSlots = neighbourIndex(
				inputSecondCharToInt(cellInput), inputFirstCharToInt(cellInput));

		List<Boolean> isAllNeighboursContainOpposite = new ArrayList<Boolean>();

		String oppositeTurn = turn.equals(constFor_X) ? constFor_O : constFor_X;

		for (String string : neighbourSlots) {
			isAllNeighboursContainOpposite
					.add(liveReversiBoard[Character.getNumericValue(string
							.charAt(0))][Character.getNumericValue(string
							.charAt(1))].equals(oppositeTurn) ? true : false);
		}

		return isAllNeighboursContainOpposite.contains(true);

	}

	private static boolean checkInputPlaceNeighbourAreAllEmpty(String cellInput) {

		List<String> neighbourSlots = neighbourIndex(
				inputSecondCharToInt(cellInput), inputFirstCharToInt(cellInput));

		List<Boolean> isAllNeighboursEmpty = new ArrayList<Boolean>();

		for (String string : neighbourSlots) {
			isAllNeighboursEmpty
					.add(liveReversiBoard[Character.getNumericValue(string
							.charAt(0))][Character.getNumericValue(string
							.charAt(1))].equals(emptySlot) ? true : false);
		}

		boolean isAllNeighbourEmpty = isAllNeighboursEmpty.stream().allMatch(
				Boolean::valueOf);

		return isAllNeighbourEmpty;

	}

	public static List<String> neighbourIndex(int tempX, int tempY) {
		List<String> neighbourSlots = new ArrayList<>();

		int tempXMinus = tempX - 1;
		int tempXPlus = tempX + 1;
		int tempYMinus = tempY - 1;
		int tempYPlus = tempY + 1;

		if (tempXMinus >= 0 && tempXMinus < 8) {
			neighbourSlots.add(tempXMinus + "" + (tempY));
		}

		if (tempXPlus >= 0 && tempXPlus < 8) {
			neighbourSlots.add(tempXPlus + "" + (tempY));
		}

		if (tempYMinus >= 0 && tempYMinus < 8) {
			neighbourSlots.add((tempX) + "" + tempYMinus);
		}

		if (tempYPlus >= 0 && tempYPlus < 8) {
			neighbourSlots.add((tempX) + "" + tempYPlus);
		}

		if (tempXMinus >= 0 && tempXMinus < 8 && tempYMinus >= 0
				&& tempYMinus < 8) {
			neighbourSlots.add(tempXMinus + "" + tempYMinus);
		}

		if (tempXPlus >= 0 && tempXPlus < 8 && tempYMinus >= 0
				&& tempYMinus < 8) {
			neighbourSlots.add(tempXPlus + "" + tempYMinus);
		}

		if (tempXMinus >= 0 && tempXMinus < 8 && tempYPlus >= 0
				&& tempYPlus < 8) {
			neighbourSlots.add(tempXMinus + "" + tempYPlus);
		}

		if (tempXPlus >= 0 && tempXPlus < 8 && tempYPlus >= 0 && tempYPlus < 8) {
			neighbourSlots.add(tempXPlus + "" + tempYPlus);
		}
		return neighbourSlots;
	}

	private static void messageResultWinner(int countO, int countX) {
		System.out.println("Result:");
		System.out.println("Black : " + countX);
		System.out.println("White : " + countO);
	}

	private static boolean checkBothCanMove(List<String> onBoardNotEmptyData) {

		Boolean currentHasNoPlaceToMoves = proceedHasMove(turn);
		Boolean opponentHasNoPlaceToMoves = proceedHasMove(turn
				.equals(constFor_O) ? constFor_X : constFor_O);

		return opponentHasNoPlaceToMoves && currentHasNoPlaceToMoves;
	}

	private static boolean proceedHasMove(String currentTurn) {
		List<String> allEmptySlot = new ArrayList<>();

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (liveReversiBoard[i][j].equals(emptySlot)) {
					allEmptySlot.add(i + "" + j);
				}
			}
		}

		Set<String> allNeighbourSlotsOfEmpty = new HashSet<String>();

		for (String string : allEmptySlot) {

			int tempX = Character.getNumericValue(string.charAt(0));
			int tempY = Character.getNumericValue(string.charAt(1));

			List<String> tempList = neighbourIndex(tempX, tempY);

			allNeighbourSlotsOfEmpty.addAll(tempList);

		}

		List<String> neighbourSlotsOfEmpty = new ArrayList<>(
				allNeighbourSlotsOfEmpty);

		neighbourSlotsOfEmpty.removeAll(allEmptySlot);

		List<Boolean> isCurrentPlayerColorAroundEmptySlot = new ArrayList<Boolean>();

		// check neighbour of empty slot is onself (not opponent color)
		for (String string : neighbourSlotsOfEmpty) {
			int tempX = Character.getNumericValue(string.charAt(0));
			int tempY = Character.getNumericValue(string.charAt(1));
			String isCurrentPlayerColor = liveReversiBoard[tempX][tempY];

			isCurrentPlayerColorAroundEmptySlot.add(isCurrentPlayerColor
					.equals(currentTurn) ? true : false);
		}

		Boolean isAllSameWithCurrentColor = isCurrentPlayerColorAroundEmptySlot
				.stream().allMatch(Boolean::valueOf);

		return isAllSameWithCurrentColor;
	}

	public static int inputSecondCharToInt(String cellInput) {
		int cRow = Character.getNumericValue(cellInput.charAt(1)) - 1;
		return cRow;
	}

	public static int inputFirstCharToInt(String cellInput) {
		Character cColChar = cellInput.charAt(0);
		cColChar = Character.toLowerCase(cColChar);
		int cCol = cColChar.equals('a') ? a : (cColChar.equals('b') ? b
				: (cColChar.equals('c') ? c : (cColChar.equals('d') ? d
						: (cColChar.equals('e') ? e : (cColChar.equals('f') ? f
								: (cColChar.equals('g') ? g : (cColChar
										.equals('h') ? h : null)))))));
		return cCol;
	}

	public static String[] convertTwoRowtoOneArr(String[][] data) {
		ArrayList<String> list = new ArrayList<String>();

		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				list.add(data[i][j]);
			}
		}
		String[] allAvailableSlotNames = list.toArray(new String[0]);
		return allAvailableSlotNames;
	}

	// output messages :
	private static void messagePlayerTurn(String player) {
		String playerString = player.equals(constFor_X) ? constFor_X_black_string
				: constFor_O_white_string;
		System.out.println("It's " + playerString + " player's turn.");
	}

	private static String messageInputPosition(String slotPlace) {
		return "Input the position of piece : <" + slotPlace + ">";
	}

	private static void messageGameOverOnePlayerWin(String player) {
		System.out.println(player + " wins the game is over");
	}

	private static void messageIllegalInput() {
		System.out.println("This position is illegal.");
	}

	private static void messageIllegalInputAgain() {
		System.out.println("Input the position of piece again.");
	}

	// this message must show after searching possible slot that can place --
	// (after
	// o\p the board)
	private static String messageIllegalNoMoreMove(String player) {
		String playerString = "";
		if (player.equals(constFor_X)) {
			playerString = constFor_X_black_string;
		} else {
			playerString = constFor_O_white_string;
		}
		return "There are no more legal moves for the " + playerString
				+ " player.";
	}

	private static void messageGameOverDraw() {
		System.out.println("There are no more legal moves for both player.");
		System.out.println("GameOver. It's a draw.");
	}

	// other methods
	private static void printCurrentSituationOfBoard() {

		System.out.println();
		System.out.println(constSpace + "a" + constSpace + "b" + constSpace
				+ "c" + constSpace + "d" + constSpace + "e" + constSpace + "f"
				+ constSpace + "g" + constSpace + "h" + constSpace + "");
		System.out.println("");
		System.out.println("1" + constSpace + liveReversiBoard[0][0]
				+ constSpace + liveReversiBoard[0][1] + constSpace
				+ liveReversiBoard[0][2] + constSpace + liveReversiBoard[0][3]
				+ constSpace + liveReversiBoard[0][4] + constSpace
				+ liveReversiBoard[0][5] + constSpace + liveReversiBoard[0][6]
				+ constSpace + liveReversiBoard[0][7] + constSpace);
		System.out.println("2" + constSpace + liveReversiBoard[1][0]
				+ constSpace + liveReversiBoard[1][1] + constSpace
				+ liveReversiBoard[1][2] + constSpace + liveReversiBoard[1][3]
				+ constSpace + liveReversiBoard[1][4] + constSpace
				+ liveReversiBoard[1][5] + constSpace + liveReversiBoard[1][6]
				+ constSpace + liveReversiBoard[1][7] + constSpace);
		System.out.println("3" + constSpace + liveReversiBoard[2][0]
				+ constSpace + liveReversiBoard[2][1] + constSpace
				+ liveReversiBoard[2][2] + constSpace + liveReversiBoard[2][3]
				+ constSpace + liveReversiBoard[2][4] + constSpace
				+ liveReversiBoard[2][5] + constSpace + liveReversiBoard[2][6]
				+ constSpace + liveReversiBoard[2][7] + constSpace);
		System.out.println("4" + constSpace + liveReversiBoard[3][0]
				+ constSpace + liveReversiBoard[3][1] + constSpace
				+ liveReversiBoard[3][2] + constSpace + liveReversiBoard[3][3]
				+ constSpace + liveReversiBoard[3][4] + constSpace
				+ liveReversiBoard[3][5] + constSpace + liveReversiBoard[3][6]
				+ constSpace + liveReversiBoard[3][7] + constSpace);
		System.out.println("5" + constSpace + liveReversiBoard[4][0]
				+ constSpace + liveReversiBoard[4][1] + constSpace
				+ liveReversiBoard[4][2] + constSpace + liveReversiBoard[4][3]
				+ constSpace + liveReversiBoard[4][4] + constSpace
				+ liveReversiBoard[4][5] + constSpace + liveReversiBoard[4][6]
				+ constSpace + liveReversiBoard[4][7] + constSpace);
		System.out.println("6" + constSpace + liveReversiBoard[5][0]
				+ constSpace + liveReversiBoard[5][1] + constSpace
				+ liveReversiBoard[5][2] + constSpace + liveReversiBoard[5][3]
				+ constSpace + liveReversiBoard[5][4] + constSpace
				+ liveReversiBoard[5][5] + constSpace + liveReversiBoard[5][6]
				+ constSpace + liveReversiBoard[5][7] + constSpace);
		System.out.println("7" + constSpace + liveReversiBoard[6][0]
				+ constSpace + liveReversiBoard[6][1] + constSpace
				+ liveReversiBoard[6][2] + constSpace + liveReversiBoard[6][3]
				+ constSpace + liveReversiBoard[6][4] + constSpace
				+ liveReversiBoard[6][5] + constSpace + liveReversiBoard[6][6]
				+ constSpace + liveReversiBoard[6][7] + constSpace);
		System.out.println("8" + constSpace + liveReversiBoard[7][0]
				+ constSpace + liveReversiBoard[7][1] + constSpace
				+ liveReversiBoard[7][2] + constSpace + liveReversiBoard[7][3]
				+ constSpace + liveReversiBoard[7][4] + constSpace
				+ liveReversiBoard[7][5] + constSpace + liveReversiBoard[7][6]
				+ constSpace + liveReversiBoard[7][7] + constSpace);
		System.out.println();
		System.out.println();
	}

}
